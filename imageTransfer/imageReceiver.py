__author__ = 'serhan'

from receiver_ui import Ui_ImageProcessor
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys
import threading
import Queue
import numpy as np
import time
import math
import socket

class serverThread (threading.Thread):
    def __init__(self, name, imageQueue):
        threading.Thread.__init__(self)
        self.name = name
        self.nickname = ""
        self.imageQueue = imageQueue
        self.sock = socket.socket()

        host = "0.0.0.0"
        port = 12345
        self.sock.bind((host, port))
        self.sock.listen(5)

    def parser(self, data):
        pass

    def run(self):
        while True:
            print "Listening."
            c, a = self.sock.accept()
            bf = QBuffer()
            data = c.recv(102400)
            image = QImage.fromData(data)
            self.imageQueue.put(image)

            if data[0:4] == "QUIT":
                c.close()
                self.sock.close()
                return

class imGui(QMainWindow):
    def __init__(self, imageQUeue, pLock):
        self.qt_app = QApplication(sys.argv)
        QWidget.__init__(self, None)

        # create the main ui
        self.iQueue = imageQUeue
        self.ui = Ui_ImageProcessor()
        self.ui.setupUi(self)
        self.imageScene = QGraphicsScene()
        self.imageQueue = imageQUeue
        self.original = None
        self.processed = None
        self.frameScaled = None
        self.imageFile = None
        self.pLock = pLock

        self.perms = None

        # start timer
        self.timer = QTimer()
        self.timer.timeout.connect(self.updateImage)
        self.timer.start(10)

        self.sceneObject = None

        self.qt_app.aboutToQuit.connect(self.cleanup)

    def cleanup(self):
        s = socket.socket()
        s.connect(("localhost", 12345))
        s.send("QUIT")

    def loadImagePressed(self):
        # load the image from a file into a QImage object
        imageFile = QFileDialog.getOpenFileName(self,
                                              'Open file',
                                              '.',
                                              'Images (*.png *.xpm '
                                              '*.jpg)' )
        if not imageFile:
            return
        with open(imageFile, 'r') as f:
            try:
                self.imageFile = imageFile
                self.original = QImage(imageFile)
                # say that an image is loaded
                print "Image loaded: " + imageFile
            except:
                print "Problem with image file: " + self.imageFile
                self.imageFile = None
            finally:
                self.processed = self.original.convertToFormat(
                    QImage.Format_RGB16)
                if self.imageFile:
                    # find the horizontal and vertical patch numgers
                    self.tmpPatchNum = (
                        self.processed.size().width() / self.patchsize,
                        self.processed.size().height() / self.patchsize )
                    self.numPatches = self.tmpPatchNum[0] * \
                                      self.tmpPatchNum[1]

                self.updateImage()

    def resetImagePressed(self):
        # return to the original image
        if not self.imageFile:
            return
        self.processed = self.original
        self.updateImage()

    def updateImage(self):
        if self.imageQueue.qsize() > 0:
            data = self.imageQueue.get()
            image = QImage(data)

            # update the visual of the image with the new processed image
            if self.sceneObject:
                self.imageScene.removeItem(self.sceneObject)
            self.imageScene.addPixmap(QPixmap.fromImage(image))
            # self.sceneObject = self.imageScene.addPixmap(
            #         QPixmap.fromImage()
            self.imageScene.update()
            self.ui.imageView.setScene(self.imageScene)

    def run(self):
        self.show()
        self.qt_app.exec_()


def main():
    # the queue should contain no more than maxSize elements
    imageQueue = Queue.Queue()
    iThread = serverThread("Server Thread", imageQueue)
    iThread.start()
    pLock = threading.Lock()

    app = imGui(imageQueue, pLock)
    app.run()

    s = socket.socket()


if __name__ == '__main__':
    main()
