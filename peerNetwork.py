# -*- coding: utf8 -*-

import os,sys
from myPeer_ui import Ui_MainWindow
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import threading
import queue
import uuid

class peerGui(QMainWindow):
    def __init__(self, appQueue, cliQueue):
        # constructor

        self.qt_app = QApplication(sys.argv)
        QWidget.__init__(self, None)

       # create the main ui
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.imageMyScene = QGraphicsScene()
        self.sceneObject = None

        # uygulamaya mesaj atmak icin gerekli kuyruk
        self.appQueue = appQueue
        self.cliQueue = cliQueue

        # get a uuid and show it
        self.uuid = self.getUUID()
        self.ui.statusBar.showMessage("UUID: " + str(self.uuid))

        #### buttons ####
        # update and show
        self.ui.buttonUpdate.clicked.connect(self.updateList)
        self.ui.buttonShow.clicked.connect(self.showUser)
        self.ui.buttonSubmit.clicked.connect(self.saveInfo)
        self.ui.buttonLoadPhoto.clicked.connect(self.loadImagePressed)
        self.ui.buttonRemove.clicked.connect(self.removeItem)
        self.ui.listWidget.doubleClicked.connect(self.showUser)
        self.ui.buttonConnect.clicked.connect(self.addConnection)

        # profil bilgilerini tutan dictionary
        self.info = {}

        # baglanti bilgileri
        # key: <shortUUID>
        # value: [ <uuid>, <IP>, <Port>, <type>, <time> ]
        self.connections = {}

        # attributes
        self.imageFile = None

        self.generateData()

    def generateData(self):
        # test amaciyla olusturuldu.

        self.ui.my_affiliation.setText("GSU")
        self.ui.my_city.setText("Istanbul")
        self.ui.my_name.setText("John Doe")
        self.ui.my_status.setText("Hello World!")
        self.loadImagePressed("/home/serhan/tux.png")


        tempUUID = str(self.getUUID())
        self.connections[tempUUID[0:8]] = [
                  tempUUID,
                  "194.27.192.80",
                  80,
                  "P",
                  1480891198,
                  "Serhan Danis",
                  "10101910",
                  "Istanbul",
                  "Bogazici University",
                  "Bir tilkinin kümese girmesinde şaşılacak bir şey "
                  "yoktur Züleyha. Lakin şaşırtıcı olan tavuğun "
                  "tilkiyi kümese davet etmesidir.",
                  "<imageInfo>"
                  ]

        tempUUID = str(self.getUUID())
        self.connections[tempUUID[0:8]] = [
                 tempUUID,
                  "194.27.192.190",
                  80,
                  "P",
                  1480891178,
                  "Ali Veli",
                  "30101930",
                  "Balıkesir",
                  "ODTÜ",
                  "Züleyha, şimdi bir tavşan sessizliği ve ceylan "
                  "çabukluğuyla geliyorsun",
                  "<imageInfo>"
                  ]

    def addConnection(self):
        # Add (New Connection) butonuna basildiginda cagrilacak
        # TODO: Validators koyulsa iyi olurdu

        if len(self.ui.connect_ip.text()) > 0 and \
                        len(self.ui.connect_port.text()) > 0:
            tempUUID = str(self.getUUID())

            new_connection = [ "-" + tempUUID[1:8],
                               self.ui.connect_ip.text(),
                               int(self.ui.connect_port.text()),
                               "",
                               ""]
            self.connections[new_connection[0]] = new_connection
            self.addItem(new_connection)

    def saveInfo(self):
        # save butonuna basildiginda calisacak
        # girilen profil bilgilerini self.info'ya yazmak icin kullaniliyor

        if self.ui.my_name.text() \
            and self.ui.my_affiliation.text() \
            and self.ui.my_city.text() \
            and self.ui.my_status.toPlainText() \
            and self.ui.my_dob.text() \
            and self.imageFile:

            self.info["uid"] = self.uuid
            self.info["nam"] = self.ui.my_name.text()
            self.info["aff"] = self.ui.my_affiliation.text()
            self.info["cit"] = self.ui.my_city.text()
            self.info["sta"] = self.ui.my_status.toPlainText()
            self.info["dob"] = self.ui.my_dob.date().toString("ddMMyyyy")
            ba = QByteArray()
            buffer = QBuffer(ba)
            self.imageFile.save(buffer, "PNG")
            self.info["img"] = buffer

            print(self.info)

            self.ui.labelSubmission.setText("Saved")
            self.ui.labelSubmission.setStyleSheet("QLabel { color : green; }")
        else:
            self.info = {}
            self.ui.labelSubmission.setStyleSheet("QLabel { color : red; }")
            self.ui.labelSubmission.setText("Invalid Data")


    def getPeerProfileInfo(self, uuid):
        # verilen uuid bilgisini gosteren baglantidan ilgili bilgileri cekmek
        #  icin kullanilabilir. Anlasilacagi uzere, bir client ile ilgili
        # baglanti adresine baglanti yapilip bilgilerin indirilmesi gerekiyor.
        pass

    def showUser(self):
        # show butonuna basildiginda calisacak
        # listeden secilmis satirin bilgilerini gerekli alanlara girmek icin
        # kullanilacak

        if len(self.ui.listWidget.selectedItems()) > 0:
             index = self.ui.listWidget.selectedIndexes()[0]
             shortUUID = self.ui.listWidget.itemFromIndex(index).text()[0:8]
        else:
            return

        # get the uuid
        line = self.connections[shortUUID][5:]

        # burada asagidaki bilgilerin elde edilmesi gerekiyor, bunun icin
        # gerekli peer ile iletisime gecilmesi gerekiyor
        self.getPeerProfileInfo("<uuid>")

        self.setPeerUUID(self.connections[shortUUID][0])
        self.setPeerName(line[0])
        self.setPeerDOB(line[1])
        self.setPeerCity(line[2])
        self.setPeerAffiliation(line[3])
        self.setPeerStatus(line[4])

    def updateList(self):
        # Update List butonuna basildiginda cagrilacak metod
        # adres listesindeki
        self.ui.listWidget.clear()
        self.listItemsRev = {}
        for line in self.connections.values():
            self.addItem(line[0:5])

    def addItem(self, listPeerProperties):
        # verilen peer bilgilerine gore listWidget'a bilgi eklenmesini saglar
            tempItem = QListWidgetItem(
                    listPeerProperties[0][0:8] + " | " +
                    listPeerProperties[1] + " | " +
                    str(listPeerProperties[2]) + " | " +
                    str(listPeerProperties[3]) + " | " +
                    str(listPeerProperties[4])
            )

            self.ui.listWidget.addItem(tempItem)

    def removeItem(self):
        # item referansi verilen baglantiyi self.connections ve
        # listWidget'tan siler
        if len(self.ui.listWidget.selectedItems()) > 0:
             item = self.ui.listWidget.selectedIndexes()[0]
             index = self.ui.listWidget.selectedIndexes()[0]
             shortUUID = self.ui.listWidget.itemFromIndex(index).text()[0:8]
        else:
            return

        del self.connections[shortUUID]
        del item

        self.updateList()

    def getUUID(self):
        # yeni bir UUID olustur
        return uuid.uuid4()

    def loadImagePressed(self, imageFile = None):
        # load the image from a file into a QImage object
        if not imageFile:
            imageFile = QFileDialog.getOpenFileName(self,
                                              'Open file',
                                              '.',
                                              'Images (*.png)' )
        if not imageFile:
            return
        with open(imageFile, 'r') as f:
            try:
                self.imageFile = QImage(imageFile)
                # say that an image is loaded
                print("Image loaded: " + imageFile)
            except:
                print("Problem with image file: " + self.imageFile)
                self.imageFile = None
            finally:
                if self.sceneObject:
                    self.imageMyScene.removeItem(self.sceneObject)

                self.sceneObject = self.imageMyScene.addPixmap(
                        QPixmap.fromImage(self.imageFile))
                self.imageMyScene.update()
                self.ui.my_image.setScene(self.imageMyScene)

    def setPeerName(self, nameText):
        # verilen metni ilgili yere yazar
        self.ui.peer_name.setText(nameText)

    def setPeerUUID(self, uuid):
        # verilen metni ilgili yere yazar
        self.ui.peer_UID.setText(uuid)

    def setPeerDOB(self, dateText):
        # verilen metni ilgili yere yazar
        myDate = QDate.fromString(dateText, "ddMMyyyy")
        self.ui.peer_dob.setDate(myDate)

    def setPeerAffiliation(self, affText):
        # verilen metni ilgili yere yazar
        self.ui.peer_affiliation.setText(affText)

    def setPeerCity(self, cityText):
        # verilen metni ilgili yere yazar
        self.ui.peer_city.setText(cityText)

    def setPeerStatus(self, statusText):
        # verilen metni ilgili yere yazar
        self.ui.peer_status.setText(statusText)

    def setPeerImage(self, data):
        data = self.imageQueue.get()
        image = QImage(data)

        # update the visual of the image with the new processed image
        if self.sceneObject:
            self.imageScene.removeItem(self.sceneObject)
        self.imageScene.addPixmap(QPixmap.fromImage(image))
        # self.sceneObject = self.imageScene.addPixmap(
        #         QPixmap.fromImage()
        self.imageScene.update()
        self.ui.image.setScene(self.imageScene)

    def run(self):
        self.show()
        self.qt_app.exec_()


def main():
    # the queue should contain no more than maxSize elements
    # QTextCodec.setCodecForCStrings(QTextCodec.codecForName("UTF-8"))

    workThreads = []
    appQueue = queue.Queue()
    cliQueue = queue.Queue()

    app = peerGui(appQueue, cliQueue)
    app.run()

    for thread in workThreads:
        thread.join()

if __name__ == '__main__':
    main()

